<?php
header('Content-Type: text/html; charset=UTF-8');

if ($_SERVER['REQUEST_METHOD'] == 'GET') {
    if (!empty($_GET['save'])) {
      print('Спасибо, результаты сохранены.');
    }
    include('form.php');
    exit();
}
  
  

try{
    
    $errors = FALSE;
    if (empty($_POST['name'])) {
        print('Заполните имя.<br/>');
        $errors = TRUE;
    }
    if (empty($_POST['e-mail'])) {
        print('Заполните почту.<br/>');
        $errors = TRUE;
    }

    if (empty($_POST['bio'])) {
        print('Заполните биографию.<br/>');
        $errors = TRUE;
    }
    if (empty($_POST['contract'])) {
        print('Вы должны быть согласны с условиями.<br/>');
        $errors = TRUE;
    }

     if (empty($_POST['date'])) {
        print('Заполните дату.<br/>');
        $errors = TRUE;
    }
    if (empty($_POST['sex'])) {
        print('Заполните пол.<br/>');
        $errors = TRUE;
    }

    if (empty($_POST['limbs'])) {
        print('Заполните конечности.<br/>');
        $errors = TRUE;
    }

    if ($errors) {
        exit();
    }


    
    $name = $_POST['name'];
$email = $_POST['e-mail'];
$date = $_POST['date'];
$sex = $_POST['sex'];
$radio2 = $_POST['limbs'];
$bio = $_POST['bio'];


     $conn = new PDO('mysql:host=localhost;dbname=u24335', 'u24335', '7834647', array(PDO::ATTR_PERSISTENT => true));

    $user = $conn->prepare("INSERT INTO users2 SET fio = ?, email = ?, yob = ?, gender = ?, n_limbs = ?, bio = ?");
    $user -> execute([$name, $email , $date, $sex , $radio2, $bio]);
    $id_user = $conn->lastInsertId();

    $abilitys = $conn->prepare("INSERT INTO abilitys SET id_user = ?");
    $abilitys -> execute([$id_user]);
    $id_abil = $conn->lastInsertId();

    $abil = implode(',',$_POST['superpowers']); 

    $ability = $conn->prepare("INSERT INTO ability SET abilitysId = ?, names_ability = ?"); 
    $ability -> execute([$id_abil, $abil]);

    header("Location: ?save=1"); 


}
    catch(PDOException $e){
    print('Error : ' . $e->getMessage());
    exit();

}
?>
